var arrayOfQuote = [ 

    {
        "author": " A. P. J. Abdul Kalam ",
        "Quote": "If you want to shine like a sun, first burn like a sun."  
    },
    {
        "author": " Albert Einstein ",
        "Quote": "If you can't explain it simply, you don't understand it well enough."
    },
    {
        "author": " Bill Gates ",
        "Quote": " Success is a lousy teacher. It seduces smart people into thinking they can't lose."
    },
    {
        "author": " Elon Musk ",
        "Quote": " When something is important enough, you do it even if the odds are not in your favor."
    },
    {
        "author": " Mother Teresa  ",
        "Quote": " Let us always meet each other with smile, for the smile is the beginning of love."
    }

]

function randomSelector(arrayLength) {
    return Math.floor(Math.random() * arrayLength);
}

function generateQuote() {

    var randomNumber = randomSelector(arrayOfQuote.length);
    document.getElementById("quoteOutput").innerHTML = ' " ' + arrayOfQuote[randomNumber].Quote + ' " ';
    document.getElementById("authorOutput").innerHTML = " - " + arrayOfQuote[randomNumber].author;
}